// Create a function that takes an array of strings and returns an array with only the strings that have numbers in them. If there are no strings containing numbers, return an empty array.

// Notes
// The strings can contain white spaces or any type of characters.
// Bonus: Try solving this without RegEx.



const sliceArr = (arr) => {
    let hasil = arr.filter(function (string) {
        return string.split('').some(function (char) {
            return !isNaN(parseInt(char))
        })
    })

    return hasil
}

console.log(sliceArr(["1a", "a", "2b", "b"]));
console.log(sliceArr(["abc", "abc10"]));
console.log(sliceArr(["abc", "ab10c", "a10bc", "bcd"]));
console.log(sliceArr(["this is a test", "test1"]));
console.log(sliceArr(['abc', 'ab10c', 'a10bc', 'bcd']));
console.log(sliceArr(['1', 'a' , ' ' ,'b']));
console.log(sliceArr(['rct', 'ABC', 'Test', 'xYz']));

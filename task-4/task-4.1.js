// Write a function that does the following for the given values: add, subtract, divide and multiply.
// These are simply referred to as the basic arithmetic operations.The variables have to be defined, but in this challenge they will be defined for you.
// All you have to do is check the variables, do some string to integer conversions, use some if conditions, and apply the arithmetic operation.
// The numbers and operation are given as strings and should result in an integer value.

const Calculate = ( num1, num2, operation) => {
    let hasil;

    const number1 = parseInt(num1);
    const number2 = parseInt(num2);

    if (operation === "add") {
       hasil = number1 + number2
    } else if (operation === "subtract") {
        hasil = number1 - number2
    } else if (operation === "divide") {
        hasil = number1 / number2
    } else if (operation === "multiply") {
        hasil = number1 * number2
    } else if (operation === "modulo") {
        hasil = number1 % number2
    } else {
        console.log("invalid operation")
    }
    
    hasil == "Infinity" ? undefined :  console.log(hasil)
}

// example :
Calculate("6", "7", "add");

Calculate(1, 2, "add");
Calculate(4, 5, "subtract");
Calculate(6, 3, "divide");
Calculate(2, 7, "multiply");
Calculate(6, 0, "divide");

// We can assign a value to each character in a word, based on their position in the alphabet(a = 1, b = 2, ... , z = 26).
// A balanced word is one where the sum of values on the left - hand side of the word equals the sum of values on the right - hand side.
// For odd length words, the middle character(balance point) is ignored.



// const balanced = (a) => {
//     let left = 0;
//     let right = 0;
//     let midpoint = Math.floor(a.length / 2);

    
//   for (let i = 0; i < midpoint; i++) {
//     right += a.charCodeAt(0) - 96;
//   }

//   for (let j = a.length - 1; j > midpoint; j--) {
//     left += a.charCodeAt(0) - 96;
//   }

//   return left === right ? true : false ;
// }


// const balanced = (str) => {
//     let a = 0;
//     let b = 0;

//     if (str.length % 2 === 0) {
//         for (let i = 0; i < str.length; i++) {
//             (i < str.length / 2 ? a += str.charCodeAt(i) : b += str.charCodeAt(i))
//         }
//     }
//     if (str.length % 2 === 0) {
//         for (let i = 0; i < str.length; i++) {
//             (i < (str.length / 2) - 1 ? a+= str.charCodeAt(i) : b += str.charCodeAt(i))
//         }
//     }

//     return a === b ? true : false

function balanced(str) {
  const sum = (acc, char) => acc + char.charCodeAt(0) - 96;
  const midpoint = Math.floor(str.length / 2);

  const leftSum = [...str.slice(0, midpoint)].reduce(sum, 0);
  const rightSum = [...str.slice(-midpoint)].reduce(sum, 0);

  return leftSum === rightSum ? true : false ;
}

console.log(balanced("zips"))
console.log(balanced("brake"))
console.log(balanced('at'))
console.log(balanced('forgetful'))
console.log(balanced('vegetation'))
console.log(balanced('disillusioned'))
console.log(balanced('abstract'))
console.log(balanced('clever'))
console.log(balanced('conditionalities'))
console.log(balanced('seasoning'))



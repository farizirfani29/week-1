// import axios from "axios";
import fetch from "node-fetch";

//  Nomer 1
// const coutdown = (time) => {
//     const interval = setInterval(() => {
//         if (time > 0) {
//             let result = new Date(time * 1000).toISOString().slice(11, 19);
//             time--
//             console.log(result)
//         } else {
//             console.log("Error")
//         }
//     }, 1000)
// }

// coutdown(5402)


// Nomer 2 
const api = await fetch('https://api.jikan.moe/v4/recommendations/anime')
    .then((res) => res.json())
    .then((res) => res.data);

// No 2.1 (Show all list title)

let showAllTitle = api.map((res) => 
    res.entry.map((res) => res.title)
).flat().slice(0, 20)
console.table(showAllTitle)


// No 2.2 (Sort title by date release)

const showTitleByDate = showAllTitle.sort((a, b) => {
    let dateA = new Date(a.date)
    let dateB = new Date(b.date)
    return dateA - dateB
})
console.table(showTitleByDate)

// Get data from another API
const getId = api.map((e) => e.entry.map((e) => e.mal_id)).flat().slice(0, 5);

let mostAnimePopularity = [];
let highRank = [];
let mostEpisodeAnime = [];
for (let i = 0; i < getId.length; i++){
    const api2 = await fetch(`https://api.jikan.moe/v4/anime/${getId[i]}`)
        .then((res) => res.json())
        .then((res) => res.data)
    
    mostAnimePopularity.push([api2.title, api2.popularity])
    highRank.push([api2.title, api2.rank])
    mostEpisodeAnime.push([api2.title, api2.episodes])
}
// 2.3 (Show 5 most popular anime from recommendation)
console.table(mostAnimePopularity)

// 2.4 (Show 5 high rank anime from recommendation)
console.table(highRank)

// 2.5 (Show most episodes anime from recommendation)
console.table(mostEpisodeAnime)
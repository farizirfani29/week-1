// const number_pairs = (a) => {

//     let sorting = a.split(' ').splice(1, a.length)
//     let obj = {};
//     let search = sorting

//     return search
//     for (let i = 0; i < a.length; i++){
//         return sorting
//     }
//     // let result = sorting.charAt(0)
//     // let slicing = a.slice(1, )
// }

// // diubah dulu jadi objek
// // menggunakan for

// console.log(number_pairs("9 10 20 20 10 10 30 50 10 20"))


function number_pairs(str) {
  const numbers = str.split(' ').map(Number);
  const count = numbers.shift();
  const obj = {};
  let result = 0;

  for (let i = 0; i < count; i++) {
    const num = numbers[i];
      obj[num]? obj[num]++ : obj[num] = 1
  }

  for (const num in obj) {
    result += Math.floor(obj[num] / 2);
  }

  return result;
}

console.log(number_pairs("7 1 2 1 2 1 3 2"));
console.log(number_pairs("9 10 20 20 10 10 30 50 10 20"));

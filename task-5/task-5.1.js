const emoji = (value) => {
    
    if (value.match("smile")) {
        return value.replace('smile', ':D')
    } else if (value.match("grin")){
        return value.replace('grin', ':)')
    } else if (value.match("sad")) {
        return value.replace('sad', ':(')
    } else if (value.match('mad')) {
        return value.replace('mad', ':P')
    } else {
        return "error"
    }
}

console.log(emoji("Make me smile"))
console.log(emoji("Make me grin"))
console.log(emoji("Make me sad"))
console.log(emoji("Make me smile"))
console.log(emoji("Make me grin"))
console.log(emoji("Make me mad"))
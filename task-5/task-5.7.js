const reverseWords = (a) => {
    let splited = a.split(' ').reverse().join(' ').replace(/\s+/g, ' ').trim()
    return splited
}

console.log(reverseWords(" the sky is blue"));
console.log(reverseWords("hello   world!  "))
console.log(reverseWords("a good example"));
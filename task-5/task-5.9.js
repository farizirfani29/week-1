const awardPrizes = (a) => {

    let hasil = {};
    let sorted = Object.entries(a).sort((a, b) => b[1] - a[1])
    for (let i = 0; i < sorted.length; i++) {
        let [name] = sorted[i]

        if (i === 0) {
            hasil[name] = "Gold"
        }else if (i === 1) {
             hasil[name] = "Silver"
        }else if (i === 2) {
             hasil[name] = "Bronze"
        } else {
            hasil[name] = "Participation"
        }
    }

    return hasil
}

console.log(awardPrizes({
  "Joshua" : 45,
  "Alex" : 39,
  "Eric" : 43
}))

console.log(awardPrizes({
  "Mario" : 99,
  "Luigi" : 100,
  "Yoshi" : 299,
  "Toad" : 2
})
)

console.log(awardPrizes({
	'Joshua' : 45,
	'Alex' : 39,
	'Eric' : 43
}))

console.log(awardPrizes({
	'Person A' : 1,
	'Person B' : 2,
	'Person C' : 3,
	'Person D' : 4,
	'Person E' : 102
})
)
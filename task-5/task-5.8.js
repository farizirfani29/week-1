const inBox = (a) => {
    for (let i = 0; i < a.length; i++){
        if (a[i][0] === "#" && a[i][a[i].length - 1] === "#" && a[i].includes("*")) {
            return true   
        }
    } return false
}

console.log(inBox([
    "###",
    "#*#",
    "###"]))
console.log(inBox([
    "####", 
    "#* #",
    "#  #",
    "####"]))
console.log(inBox([
    "####",
    "# #",
    "#  #*",
    "####"]))
console.log(inBox([
    "#####",
    "#   #",
    "#   #",
    "#   #",
    "#####"]))
console.log(inBox([
"#####", 
"#   #", 
"#   #", 
"# * #", 
"#####"
]))

console.log(inBox([
"#####", 
"#*  #", 
"#   #", 
"#   #", 
"#####"
]))

console.log(inBox([
  "*####",
  "# #",
  "#  #*",
  "####"
])
)
const missingNum = (a) => {

    const total = 55 - a.reduce((a, b) => a + b)
    return total
}

console.log((missingNum([1 ,2 ,3 ,4 ,6 ,7 ,8 ,9 ,10 ])))
console.log((missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9])))
console.log((missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8])))
console.log(missingNum([7, 2, 3, 9, 4, 5, 6, 8, 10]))
console.log(missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9]))
console.log(missingNum([1, 7, 2, 4, 8, 10, 5, 6, 9]))
const isSpecialArray = (a) => {
    return a.every((item, index) => item % 2 === index % 2);
}

console.log(isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3])) // true
console.log(isSpecialArray([1, 1, 1, 2])) // false
console.log(isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3])) // false
console.log(isSpecialArray([2, 1, 2, 1]) )
const removeFunction = (a) => {
    const deleted = [...new Set(a)]

    return deleted
}

console.log(removeFunction([1,0,1,0]))
console.log(removeFunction(["The", "big", "cat"]))
console.log(removeFunction(["John", "Taylor", "John"]))
console.log(removeFunction(["The", "big", "cat"]))
console.log(removeFunction(['John', 'Taylor', 'John', 'john']))
console.log(removeFunction(['javascript', 'python', 'python', 'ruby', 'javascript', 'c', 'ruby']))
console.log(removeFunction(['#', '#', '%', '&', '#', '$', '&']))
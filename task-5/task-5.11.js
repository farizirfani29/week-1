const calculateScore = (games) => {
    let scoreAbigail = 0;
    let scoreBenson = 0

    for (let i = 0; i < games.length; i++) {
        const [abigail, benson] = games[i];

        if (
            (abigail === 'R' && benson === 'S') ||
            (abigail === 'S' && benson === 'P') ||
            (abigail === 'P' && benson === 'R')
        ) {
            scoreAbigail++
        } else if (
            (benson === 'R' && abigail === 'S') ||
            (benson === 'S' && abigail === 'P') ||
            (benson === 'P' && abigail === 'R')
        ) {
            scoreBenson++
        }
    }
    
    if (scoreAbigail < scoreBenson) {
        return 'Abigail'
    } else if (scoreAbigail > scoreBenson) {
        return 'Benson'
    } else {
        return 'Tie'
    }
}


console.log(calculateScore([["R", "P"], ["R", "S"], ["S", "P"]]));
console.log(calculateScore([["R", "R"], ["S", "S"]]));
console.log(calculateScore([["S", "R"], ["R", "S"], ["R", "R"]]));
console.log(calculateScore([['R', 'P'], ['R', 'S'], ['S', 'P']]));
console.log(calculateScore([['R', 'R'], ['S', 'S']]));
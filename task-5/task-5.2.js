const makeTitle = (a) => {
    const split = a.split(" ")

    const convert = split.map((str) => {
      return str.charAt(0).toUpperCase() + str.slice(1);
    })
    return convert.join(" ")
}

console.log(makeTitle("this is a title"))
console.log(makeTitle("capitalize every word"))
console.log(makeTitle("PIZZA PIZZA PIZZA"))
console.log(makeTitle("I am a title"))
console.log(makeTitle("I AM A TITLE"))
// 1
function myFunction(a, b) {
    return a + b;
}

// 2
function myFunction(a, b) {
    return a === b;
}

// 3
function myFunction(a) {
    return typeof (a);
}

// 4
function myFunction(a, n) {
    return charAt[a-n]
}

// 5
function myFunction (a) {
    return a.slice(3)
}

// 6
function myFunction(str) {
   return str.slice(-3);
}

// 7
function myFunction(a) {
   return a.substring(0, 3);
}

// 8
function myFunction(a) {
   return a.indexOf('is');
}

// 9
function myFunction(a) {
   if (str.length % 2 == 0) {
    return str.slice(0, str.length / 2);
  }
}

// 10
function myFunction(a) {
   return a.slice(0, -3);
}

// 11 
function myFunction(a, b) {
  return (a / 100) * b
}

// 12
function myFunction(a, b, c, d, e, f) {
   return (((a + b - c) * d) / e) ** f;
}

// 13
function myFunction(a, b) {
    return a.includes(b) ? b + a : a + b
}

// 14
function myFunction(a) {
  return a % 2 == 0 ? true : false;
}

// 15
function myFunction(a, b) {
  return b.split(a).length - 1
}

// 16
function myFunction(a) {
    // return a % 2 === 0 ? true : false
    return parseInt(a) === a
}

// 17
function myFunction(a, b) {
    return a > b ? a * b : a / b
    // return a < b ? a / b : a * b
}

// 18 
function myFunction(a) {
  return Number(a.toFixed(2));
}

// 19
function myFunction(a) {
    return Array.from(String(a), Number);
}


// =========================================


// 1
function myFunction(a, n) {
   return a[n - 1];
}

// 2

function myFunction(a) {
   return a.slice(3);
}

// 3
function myFunction(a) {
   return a.slice(-3);
}

// 4
function myFunction(a) {
   return a.slice(0, 3);
}

// 5
function myFunction(a, n) {
    //a.slice(Math.max(a.length - n, 0));  
     return a.slice(-n);
}

// 6
function myFunction( a, b ) {
  return a.filter(c => c !== b)
}

// 7
function myFunction(a) {
   return a.length;
}

// 8
function myFunction(a) {
    return a.filter((v) => v < 0).length
}

// 9 
function myFunction(arr) {
    return arr.sort()
}


//
// 10
function myFunction(arr) {
    return arr.sort().reverse()
}

// 11
function myFunction(arr) {
    return arr.reduce((x, y) => {
        return x + y
    }
)}

// 12
function myFunction(arr) {
    return arr.reduce((a, b) => a + b) / arr.length;
}

// 13
function myFunction(arr) {
    return arr.reduce((a, b) => a.length <= b.length ? b : a )
}

// 14
function myFunction( arr ) {
  return arr.every((a) => a === arr[0])
}

// 15
function myFunction( ...arrays ) {
return arrays.flat()
}

// 16

function myFunction(arr) {
   return arr.sort((a, b) => a.a - b.b)
}


// 17
function myFunction(a, b) {
  return [...new Set([...a, ...b])].sort((x, y) => x - y);
}


// ===================================================


// 1
function myFunction(obj) {
  return obj.country
}

// 2
function myFunction(obj) {
  return obj['prop-2']
}

// 3
function myFunction(obj, key) {
  return obj[key]
}

// 4
function myFunction(a, b) {
    return a.hasOwnProrperty(b)
}

// 5
function myFunction(a, b) {
    return a[b] ? true : false;
}

// 6
function myFunction(a) {
   return { key: a };
}

// 7
function myFunction(a, b) {
  return { [a]: b }
}

// 8
function myFunction(a, b) {
    return a.reduce((x, y, i) => ({ ...x, [y]: b[i] }), {})
}

// 9 
function myFunction(a) {
    return Object.keys(a);
}

// 10 
function myFunction(obj) {
    return obj?.a?.b;
}

// 11
function myFunction(a) {
    return Object.values(a).reduce((x,y) => x + y, 0)
}

// 12
function myFunction(obj) {
    return delete obj.b && obj;
}

// 13
function myFunction(x, y) {
    y.d = y.b;
    delete y.b;
    return {...x, ...y};
}

// 14
function myFunction(a, b) {
    return Object.entries(a).reduce((pv, cv) => ({...pv, [cv[0]]: cv[1] * b}), {});
}
